//Heather Chan, Uriel Renteria, Visa Touch, Thien An Phan, Manh Khang Nguyen, Geovanny Casian, Angie Campos
//3-2-21
//Chapter 3 Assignments - Tic-Tac-Toe, Tower of Hanoi, and n-Queens

#include <iostream>
#include <ctime>
#include "input.h"
#include "nQueens.h"
#include "queensFunctions.h"
#include "Towers_of_Hanoi.h"
#include "tictactoe.h"
#include "Hanoi_tower.h"
using namespace std;

//Precondition: N/A
//Postcondition: begins nQueens game using class nQueens
void queensGame();

//Precondition: N/A
//Postcondition: begins towers of hanoi game using class Towers_of_Hanoi
void towerOfHanoi();

//Precondition: N/A
//Postcondition: begins tic-tac-toe game using class TicTacToe
void ticTacToeGame();

//Precondition: currentTime contains the time in seconds to be compared
//Postcondition: if current time is faster or slower than fastestTime and slowestTime, values in fastestTime and slowestTime
//are changed accordingly.
void checkTimes(time_t& fastestTime, time_t& slowestTime, time_t currentTime);

int main()
{
	using namespace inputNamespace;
	system("cls");
	int input;
	do
	{
		cout << "\n\tCMPR131 - Game Applications\n";
		cout << "\t============================================================================\n";
		cout << "\t\t1) Tic-Tac-Toe\n";
		cout << "\t\t2) Tower of Hanoi\n";
		cout << "\t\t3) n-Queens\n";
		cout << "\t----------------------------------------------------------------------------\n";
		cout << "\t\t0) Exit\n";
		cout << "\t============================================================================\n";

		input = inputInteger("\t\tOption: ", 0, 3);
		switch (input)
		{
		case 0:
			exit(0);
		case 1:
			ticTacToeGame();
			break;
		case 2:
			towerOfHanoi();
			break;
		case 3:
			queensGame();
			break;
		default:
			cout << "\tError: invalid input. Please try again.";
			break;
		}
	} while (true);
}

void queensGame()
{
	using namespace inputNamespace;
	using namespace queensNamespace;

	//variables for statistic tracking
	time_t fastestTime = LONG_MAX, slowestTime = 0, gameTime;
	bool gameWon = false, statsTracked = false;

	cout << endl << LEFT_ALIGNMENT << "The n-queens puzzle is the problem of placing n chess queens on a nxn chessboard\n"
		<< LEFT_ALIGNMENT << "so that no two queens threaten each other; thus, a solution requires that no two\n"
		<< LEFT_ALIGNMENT << "queens share the same row, column, or diagonal. Solutions exist for all natural\n"
		<< LEFT_ALIGNMENT << "numbers n with the exception of n = 2 and n = 3.\n\n";

	bool continueGame;
	do
	{
		size_t boardSize = inputInteger(LEFT_ALIGNMENT + "Enter the board dimension nxn: ", true);
		nQueens queensGame(boardSize);

		playGame(queensGame, gameTime, gameWon); //gameTime and gameWon are changed to the correct values in the function
		
		if (gameWon == true)
		{
			checkTimes(fastestTime, slowestTime, gameTime);
			statsTracked = true;
		}

		char userAnswer = toupper(inputChar(LEFT_ALIGNMENT + "Play again? (Y-yes or N-no)  ", 'Y', 'N'));
		if (userAnswer == 'Y')
			continueGame = true;
		else
			continueGame = false;
	} while (continueGame);

	//on exit, check statistics
	if (statsTracked == true)
	{
		cout << "\n\t\tThe fastest game was " << fastestTime << " seconds.";
		cout << "\n\t\tThe slowest game was " << slowestTime << " seconds.\n";
	}
	else
	{
		cout << "\n\t\tNo win statistics available for this game session.\n";
	}

	system("pause");
	return;
}

void towerOfHanoi()
{
	using namespace hanoiNamespace;

	//variables for statistic tracking
	time_t fastestTime = LONG_MAX, slowestTime = 0, gameTime;
	bool gameWon = false, statsTracked = false;

	cin.ignore(999, '\n');
	bool play_more = true;
	do
	{
		system("cls");
		Towers_of_Hanoi Game_Set;
		bool win_condition = true;
		while (win_condition)
		{
			Game_Set.print_tower();
			win_condition = Game_Set.move_disk(gameTime, gameWon);
		}
		if (gameWon == true)
		{
			checkTimes(fastestTime, slowestTime, gameTime);
			statsTracked = true;
		}
		play_more = Game_Set.play_again();
	} while (play_more);

	//on exit, check statistics
	if (statsTracked == true)
	{
		cout << "\n\t\tThe fastest game was " << fastestTime << " seconds.";
		cout << "\n\t\tThe slowest game was " << slowestTime << " seconds.\n";
	}
	else
	{
		cout << "\n\t\tNo win statistics available for this game session.\n";
	}

	return;
}

void ticTacToeGame()
{
	using namespace tictactoeNamespace;

	//handles statistics inside class
	TicTacToe startGame;
	startGame.start_game();
	return;
}

void checkTimes(time_t& fastestTime, time_t& slowestTime, time_t currentTime)
{
	if (currentTime < fastestTime) //if current time is less than fastest time, set fastest time to current time
		fastestTime = currentTime;
	if (currentTime > slowestTime) //if current time is greater than slowest time, set slowest time to current time
		slowestTime = currentTime;
	return;
}