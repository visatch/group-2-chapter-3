//Manh Khang Nguyen, Uriel Renteria

#pragma once
#include <iostream>
#include <cmath> 
using namespace std;

namespace queensNamespace
{
	typedef int* int_ptr;
	typedef int** int_ptr_2D;

	const string LEFT_ALIGNMENT = "\t\t";
	const char TOP_LEFT_CORNER = char(201);
	const char TOP_RIGHT_CORNER = char(187);
	const char BOTTOM_LEFT_CORNER = char(200);
	const char BOTTOM_RIGHT_CORNER = char(188);
	const char DOUBLE_LINE_HORIZONTAL = char(205);
	const char DOUBLE_LINE_VERTICAL = char(186);
	const char SINGLE_LINE_HORIZONTAL = char(95);
	const char SINGLE_LINE_VERTICAL = char(179);
	const char QUEEN = 'Q';
	const char BLANK = char(32);
	const char SHADED = char(254);
	const int QUEEN_VALUE = -1;

	class nQueens
	{
	private:
		size_t size;
		int_ptr_2D board;
		unsigned int moveCounter;
		unsigned int queenCounter;

	public:

		// Precondition(s): None
		// Postcondition(s): Initializes the default constructor
		nQueens();

		// Precondition(s): None
		// Postcondition(s): Initializes constructor with size
		nQueens(const int& newSize);

		// Precondition(s): None
		// Postcondition(s): Calls the destructor
		~nQueens();

		// Precondition(s): None
		// Postcondition(s): Returns the size
		size_t getSize(void) const;

		// Precondition(s): None
		// Postcondition(s): Sets the game to a with another size board
		void setSize(const int& newSize);

		// Precondition(s): None
		// Postcondition(s): Returns the moveCount
		int getMoveCount(void) const;

		// Precondition(s): None
		// Postcondition(s): Returns the queenCount
		int getQueenCount(void) const;

		// Precondition(s): The place must not have an existing queen nor be attacked by a queen
		// Postcondition(s): Sets the queen at the rowIndex and columnIndex location
		void setQueen(const int& rowIndex, const int& columnIndex);

		// Precondition(s): The place must have an existing queen
		// Postcondition(s): Removes the queen at the rowIndex and columnIndex location
		void removeQueen(const int& rowIndex, const int& columnIndex);

		// Precondition(s): rowTargetIndex exists on the board
		// Postcondition(s): Sets the row to be attacked by the queen at given location
		void setHorizontal(const int& rowTargetIndex);

		// Precondition(s): rowTargetIndex and columnTargetIndex exist on the board
		// Postcondition(s): Sets the column to be attacked by the queen at given location
		void setVertical(const int& columnTargetIndex);

		// Precondition(s): rowTargetIndex and columnTargetIndex exist on the board
		// Postcondition(s): Sets the diagonal attacked by the queen at given location
		void setDiagonal(const int& rowTargetIndex, const int& columnTargetIndex);

		// Precondition(s): rowTargetIndex exists on the board
		// Postcondition(s): Resets the row attacked by the queen at given location
		void resetHorizontal(const int& rowTargetIndex);

		// Precondition(s): columnTargetIndex exists on the board
		// Postcondition(s): Resets the column to be attacked by the queen at given location
		void resetVertical(const int& columnTargetIndex);

		// Precondition(s): rowTargetIndex and columnTargetIndex exists on the board
		// Postcondition(s): Resets the diagonal attacked by the queen at given location
		void resetDiagonal(const int& rowTargetIndex, const int& columnTargetIndex);

		// Precondition(s): None
		// Postcondition(s): Allocates the width needed inside the board without borders
		size_t columnAllocatedSpace(void) const;

		// Precondition(s): None
		// Postcondition(s): Prints the board game with borders and objects
		void printBoard(void) const;

		// Precondition(s): None
		// Postcondition(s): Prints the board game's top border
		void printTopBorder(void) const;

		// Precondition(s): None
		// Postcondition(s): Prints the board game's body
		void printBody(void) const;

		// Precondition(s): rowIndex exists on the board
		// Postcondition(s): Print a row of the game's body
		void printRows(const int& rowIndex) const;

		// Precondition(s): None
		// Postcondition(s): Print the board game's bottom border
		void printBottomBorder() const;

		// Precondition(s): None
		// Postcondition(s): Converts numeric value of the board to char value
		char valueToObject(const int& value) const;

		// Precondition(s): rowIndex and columnIndex exist on the board
		// Postcondition(s): Returns true if the location is attacked by a queen
		bool isAttacked(const int& rowIndex, const int& columnIndex) const;

		// Precondition(s): rowIndex and columnIndex exist on the board
		// Postcondition(s): Returns true if the location has a queen
		bool isQueen(const int& rowIndex, const int& columnIndex) const;

		// Precondition(s): The numbers of queen has to equal to n, the size of the board
		// Postcondition(s): Returns true if n(size) queens are placed and all places on the board are attacked
		bool isSolved(void) const;
	};
}