#pragma once
#include "queensFunctions.h"
using namespace inputNamespace;

namespace queensNamespace
{
    char gameMenuOption(void)
    {
        cout << endl;
        cout << LEFT_ALIGNMENT << "Game Options" << "\n";
        cout << LEFT_ALIGNMENT << string(50, char(205)) << "\n";
        cout << LEFT_ALIGNMENT << "A> Place a queen\n";
        cout << LEFT_ALIGNMENT << "B> Remove an existing queen\n";
        cout << LEFT_ALIGNMENT << string(50, char(196)) << "\n";
        cout << LEFT_ALIGNMENT << "0> Exit\n";
        cout << LEFT_ALIGNMENT << string(50, char(205)) << "\n";

        cin.clear();
        char option = inputChar(LEFT_ALIGNMENT + "Option: ", string("0AB"));
        cin.clear();
        cin.ignore(999, '\n');
        cout << endl;
        return option;
    }

    void playGame(nQueens& currentGame, time_t& gameTime, bool& isWin)
    {
        bool continueLoop = true;

        time_t endTime, startTime = time(0);
        isWin = false;

        do
        {
            currentGame.printBoard();

            switch (gameMenuOption())
            {
            case '0': continueLoop = false; break;
            case 'a': case 'A': place_a_queen(currentGame); break;
            case 'b': case 'B': remove_a_queen(currentGame); break;
            default: cout << LEFT_ALIGNMENT << "ERROR-Invalid Option. Please re-enter."; break;
            }

            if (currentGame.isSolved())
            {
                cout << endl << LEFT_ALIGNMENT << "Congratulations! You have solved "
                    << currentGame.getSize() << "-Queens in " << currentGame.getMoveCount() << " moves.\n";
                endTime = time(0);
                gameTime = (endTime - startTime);
                isWin = true;
                continueLoop = false;
                currentGame.printBoard();
            }

            cout << "\n";
        } while (continueLoop);
    }

    void place_a_queen(nQueens& object)
    {
        int size = object.getSize();

        int row = inputInteger(LEFT_ALIGNMENT + "Position a queen in the row (1.." + to_string(size) + "): ", 1, size);
        int column = inputInteger(LEFT_ALIGNMENT + "Position a queen in the column (1.." + to_string(size) + "): ", 1, size);

        if (object.isQueen(row - 1, column - 1))
            cout << LEFT_ALIGNMENT << "ERROR: A queen has already placed in the position (row and column). Try again.\n";
        else if (object.isAttacked(row - 1, column - 1))
            cout << LEFT_ALIGNMENT << "ERROR: Conflict with queens in existing row. Try again.\n";
        else
            object.setQueen(row - 1, column - 1);
    }

    void remove_a_queen(nQueens& object)
    {
        int size = object.getSize();

        int row = inputInteger(LEFT_ALIGNMENT + "Position a queen in the row (1.." + to_string(size) + "): ", 1, size);
        int column = inputInteger(LEFT_ALIGNMENT + "Position a queen in the column (1.." + to_string(size) + "): ", 1, size);

        if (object.isQueen(row - 1, column - 1))
            object.removeQueen(row - 1, column - 1);
        else
            cout << LEFT_ALIGNMENT << "ERROR: No such queen existed. Try again.\n";
    }
}

