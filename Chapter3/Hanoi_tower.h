//Geovanny Casian, Angie Campos

#pragma once
#include <vector>
using namespace std;

namespace hanoiNamespace
{
	class Hanoi_Tower
	{
	public:
		// precondition: none
		// postcondition:: none
		Hanoi_Tower();

		// precondition: none
		// postcondition:: creates tower with specified amount of disks
		Hanoi_Tower(int disk_amount);

		// precondition: none
		// postcondition:: none
		~Hanoi_Tower();
		// precondition: none
		// postcondition: return value of disk at the top of the tower
		int top_disk_value(void)const;

		// precondition: none
		// postcondition: remove top disk and change amount of disks in tower
		void remove_top_disk(void);

		// precondition: none
		// postcondition: return bool for is tower empty
		bool is_tower_empty(void);

		// precondition: none
		// postcondition: add given disk to top of tower
		void add_disk_to_top(int new_disk);

		// precondition: none
		// postcondition: bool if you can place disk
		bool can_place_disk(int new_disk);

		// precondition: none
		// postcondition: return bool for if win condition is met
		bool win_condition(int disks_in_game) const;

		// precondition: none
		// postcondition: return disk at given location
		int disk_at_location(int location) const;

		// precondition: none
		// postcondition: return number of disks on tower
		int disk_on_tower()const;

		// precondition: none
		// postcondition: return number of disks on tower
		void empty_tower();
	private:
		int amount_of_disks = 0;
		vector<int> disks;
	};
}

