#pragma once
#include "nQueens.h"

// INVARIANT for the nQueen class
// 1. The value in the board array represents how many queens are currently attacking the same spot
// 2. Because positive values are for the place attacking by the queen, Queen value is set to -1
//    to differentiate it when printing

namespace queensNamespace
{
	nQueens::nQueens()
	{
		size = 1;
		board = new int_ptr[size];

		moveCounter = 0;
		queenCounter = 0;

		for (int rowIndex = 0; rowIndex < size; ++rowIndex)
			board[rowIndex] = new int[size];

		board = { 0 };

	}

	nQueens::nQueens(const int& newSize)
	{
		size = newSize;
		board = new int_ptr[size];

		moveCounter = 0;
		queenCounter = 0;

		for (int rowIndex = 0; rowIndex < size; ++rowIndex)
			board[rowIndex] = new int[size];

		for (int rowIndex = 0; rowIndex < size; ++rowIndex)
			for (int columnIndex = 0; columnIndex < size; ++columnIndex)
				board[rowIndex][columnIndex] = 0;
	}

	nQueens::~nQueens()
	{
		delete[] board;
	}

	size_t nQueens::getSize(void) const
	{
		return size;
	}

	void nQueens::setSize(const int& newSize)
	{
		size = newSize;
		delete[] board;

		board = new int_ptr[size];

		for (int rowIndex = 0; rowIndex < size; ++rowIndex)
			board[rowIndex] = new int[size];

		board = { 0 };
	}

	int nQueens::getMoveCount(void) const
	{
		return moveCounter;
	}

	int nQueens::getQueenCount(void) const
	{
		return queenCounter;
	}

	void nQueens::setQueen(const int& rowIndex, const int& columnIndex)
	{
		if (isAttacked(rowIndex, columnIndex) || isQueen(rowIndex, columnIndex))
			return;

		board[rowIndex][columnIndex] = QUEEN_VALUE;
		setHorizontal(rowIndex);
		setVertical(columnIndex);
		setDiagonal(rowIndex, columnIndex);
		++moveCounter;
		++queenCounter;
	}

	void nQueens::removeQueen(const int& rowIndex, const int& columnIndex)
	{
		if (!isQueen(rowIndex, columnIndex))
			return;

		board[rowIndex][columnIndex] = 0;
		resetHorizontal(rowIndex);
		resetVertical(columnIndex);
		resetDiagonal(rowIndex, columnIndex);
		++moveCounter;
		--queenCounter;
	}

	void nQueens::setHorizontal(const int& rowTargetIndex)
	{
		for (unsigned int columnIndex = 0; columnIndex < size; ++columnIndex)
			if (board[rowTargetIndex][columnIndex] >= 0)
				++board[rowTargetIndex][columnIndex];
	}

	void nQueens::resetHorizontal(const int& rowTargetIndex)
	{
		for (unsigned int columnIndex = 0; columnIndex < size; ++columnIndex)
			if (board[rowTargetIndex][columnIndex] > 0)
				--board[rowTargetIndex][columnIndex];
	}

	void nQueens::setVertical(const int& columnTargetIndex)
	{
		for (unsigned int rowIndex = 0; rowIndex < size; ++rowIndex)
			if (board[rowIndex][columnTargetIndex] >= 0)
				++board[rowIndex][columnTargetIndex];
	}

	void nQueens::resetVertical(const int& columnTargetIndex)
	{
		for (unsigned int rowIndex = 0; rowIndex < size; ++rowIndex)
			if (board[rowIndex][columnTargetIndex] > 0)
				--board[rowIndex][columnTargetIndex];
	}

	void nQueens::setDiagonal(const int& rowTargetIndex, const int& columnTargetIndex)
	{
		for (int rowIndex = 0; rowIndex < size; ++rowIndex)
		{
			for (int columnIndex = 0; columnIndex < size; ++columnIndex)
			{
				int verticalDistace = abs(rowTargetIndex - rowIndex);
				int horizontalDistance = abs(columnTargetIndex - columnIndex);

				if (horizontalDistance == verticalDistace)
				{
					if (board[rowIndex][columnIndex] >= 0)
						++board[rowIndex][columnIndex];
				}
			}
		}
	}

	void nQueens::resetDiagonal(const int& rowTargetIndex, const int& columnTargetIndex)
	{
		for (int rowIndex = 0; rowIndex < size; ++rowIndex)
		{
			for (int columnIndex = 0; columnIndex < size; ++columnIndex)
			{
				int verticalDistace = abs(rowTargetIndex - rowIndex);
				int horizontalDistance = abs(columnTargetIndex - columnIndex);

				if (horizontalDistance == verticalDistace)
				{
					if (board[rowIndex][columnIndex] > 0)
						--board[rowIndex][columnIndex];
				}
			}
		}
	}

	size_t nQueens::columnAllocatedSpace(void) const
	{
		return 2 * size - 1;
	}

	void nQueens::printBoard() const
	{
		cout << endl << LEFT_ALIGNMENT << size << "-Queens" << endl;
		printTopBorder();
		printBody();
		printBottomBorder();
		cout << endl;
	}

	void nQueens::printTopBorder(void) const
	{
		cout << LEFT_ALIGNMENT << TOP_LEFT_CORNER;
		for (unsigned int i = 0; i < columnAllocatedSpace(); ++i)
			cout << DOUBLE_LINE_HORIZONTAL;
		cout << TOP_RIGHT_CORNER << endl;
	}

	void nQueens::printBody(void) const
	{
		for (unsigned int rowIndex = 0; rowIndex < size; ++rowIndex)
			printRows(rowIndex);
	}

	void nQueens::printRows(const int& rowIndex) const
	{
		cout << LEFT_ALIGNMENT << DOUBLE_LINE_VERTICAL;
		for (int columnIndex = 0; columnIndex < size; ++columnIndex)
		{
			if (!isAttacked(rowIndex, columnIndex) && !isQueen(rowIndex, columnIndex)
				&& size != 1 && rowIndex != (size - 1))
				cout << SINGLE_LINE_HORIZONTAL;
			else
				cout << valueToObject(board[rowIndex][columnIndex]);

			if (columnIndex == size - 1)
				cout << DOUBLE_LINE_VERTICAL << endl;
			else
				cout << SINGLE_LINE_VERTICAL;
		}
	}


	void nQueens::printBottomBorder(void) const
	{
		cout << LEFT_ALIGNMENT << BOTTOM_LEFT_CORNER;
		for (unsigned int i = 0; i < columnAllocatedSpace(); ++i)
			cout << DOUBLE_LINE_HORIZONTAL;
		cout << BOTTOM_RIGHT_CORNER << endl;
	}

	char nQueens::valueToObject(const int& value) const
	{
		if (value == QUEEN_VALUE)
			return QUEEN;
		else if (value >= 1)
			return SHADED;
		else
			return BLANK;
	}

	bool nQueens::isAttacked(const int& rowIndex, const int& columnIndex) const
	{
		return (board[rowIndex][columnIndex] > 0);
	}

	bool nQueens::isQueen(const int& rowIndex, const int& columnIndex) const
	{
		return (board[rowIndex][columnIndex] == QUEEN_VALUE);
	}

	bool nQueens::isSolved(void) const
	{
		if (queenCounter < int(size))
			return false;

		for (int rowIndex = 0; rowIndex < size; ++rowIndex)
			for (int columnIndex = 0; columnIndex < size; ++columnIndex)
				if (board[rowIndex][columnIndex] == 0)
					return false;
		return true;
	}
}