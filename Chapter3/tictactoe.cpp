﻿#include "tictactoe.h"
#include "input.h"
#include "tictactoe.h"
#include <algorithm>
#include <iostream>
#include <numeric>
#include <ctime>

using namespace std;
using namespace inputNamespace;

const char BLANK_TOKEN = ' ';
const char HUMAN_TOKEN = 'X';
const char COMPUTER_TOKEN = 'O';

namespace tictactoeNamespace
{
	TicTacToe::TicTacToe()
	{
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				board[i][j] = ' ';
			}
		}
		humanAlreadyMoved = 0;
		gamePlayed = 0;
	}

	void TicTacToe::computerMove()
	{
		cout << "\n\n\tDumb AI moves...";

		if (humanAlreadyMoved == 1)
		{
			if (emptySpace(1, 1))
				board[1][1] = COMPUTER_TOKEN;
			else
				board[0][0] = COMPUTER_TOKEN;
			return;
		}

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 2; j++)
			{
				if (board[j][i] == COMPUTER_TOKEN && board[j + 1][i] == COMPUTER_TOKEN) // Vertical line
				{
					if (emptySpace(j + 2, i))
					{
						board[j + 2][i] = COMPUTER_TOKEN;
						return;
					}
				}

				if (board[i][j] == COMPUTER_TOKEN && board[i][j + 1] == COMPUTER_TOKEN) // Horizontal line
				{
					if (j == 0)
					{
						if (emptySpace(i, j + 2))
						{
							board[i][j + 2] = COMPUTER_TOKEN;
							return;
						}
					}
					else if (j == 1)
					{
						if (emptySpace(i, j - 1))
						{
							board[i][j - 1] = COMPUTER_TOKEN;
							return;
						}
					}
				}
			}
			if (board[i][0] == COMPUTER_TOKEN && board[i][2] == COMPUTER_TOKEN)
			{
				if (emptySpace(i, 1))
				{
					board[i][1] = COMPUTER_TOKEN;
					return;
				}
			}
			if (board[0][i] == HUMAN_TOKEN && board[2][i] == HUMAN_TOKEN)
			{
				if (emptySpace(1, i))
				{
					board[1][i] = COMPUTER_TOKEN;
					return;
				}
			}
		}

		//Check if the Human is in the middle [1,1]
		if (board[1][1] == HUMAN_TOKEN)
		{
			if (board[0][0] == HUMAN_TOKEN && board[1][1] == HUMAN_TOKEN)
			{
				if (emptySpace(2, 2))
				{
					board[2][2] = COMPUTER_TOKEN;
					return;
				}
			}
			else if (board[1][1] == HUMAN_TOKEN && board[2][2] == HUMAN_TOKEN)
			{
				if (emptySpace(0, 0))
				{
					board[0][0] = COMPUTER_TOKEN;
					return;
				}
			}
			else if (board[0][2] == HUMAN_TOKEN && board[1][1] == HUMAN_TOKEN)
			{
				if (emptySpace(2, 0))
				{
					board[2][0] = COMPUTER_TOKEN;
					return;
				}
			}
			else if (board[1][1] == HUMAN_TOKEN && board[2][0] == HUMAN_TOKEN)
			{
				if (emptySpace(0, 2))
				{
					board[0][2] = COMPUTER_TOKEN;
					return;
				}
			}
		}

		//[0,2] [1,1]
		if (board[1][1] == COMPUTER_TOKEN)
		{
			if (board[0][0] == COMPUTER_TOKEN && board[1][1] == COMPUTER_TOKEN)
			{
				if (emptySpace(2, 2))
				{
					board[2][2] = COMPUTER_TOKEN;
					return;
				}
			}
			else if (board[1][1] == COMPUTER_TOKEN && board[2][2] == COMPUTER_TOKEN)
			{
				if (emptySpace(0, 0))
				{
					board[0][0] = COMPUTER_TOKEN;
					return;
				}
			}
			else if (board[0][2] == COMPUTER_TOKEN && board[1][1] == COMPUTER_TOKEN)
			{
				if (emptySpace(2, 0))
				{
					board[2][0] = COMPUTER_TOKEN;
					return;
				}
			}
			else if (board[1][1] == COMPUTER_TOKEN && board[2][0] == COMPUTER_TOKEN)
			{
				if (emptySpace(0, 2))
				{
					board[0][2] = COMPUTER_TOKEN;
					return;
				}
			}
		}

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 2; j++)
			{
				if (board[i][j] == HUMAN_TOKEN && board[i][j + 1] == HUMAN_TOKEN) //Horizontal Line
				{
					if (j == 0)
					{
						if (emptySpace(i, j + 2))
						{
							board[i][j + 2] = COMPUTER_TOKEN;
							return;
						}
					}
					else if (j == 1)
					{
						if (emptySpace(i, j - 1))
						{
							board[i][j - 1] = COMPUTER_TOKEN;
							return;
						}
					}
				}
				if (board[j][i] == HUMAN_TOKEN && board[j + 1][i] == HUMAN_TOKEN)
				{
					if (emptySpace(j + 2, i))
					{
						board[j + 2][i] = COMPUTER_TOKEN;
						return;
					}
				}
			}

			if (board[i][0] == HUMAN_TOKEN && board[i][2] == HUMAN_TOKEN)
			{
				if (emptySpace(i, 1))
				{
					board[i][1] = COMPUTER_TOKEN;
					return;
				}
			}
			if (board[0][i] == HUMAN_TOKEN && board[2][i] == HUMAN_TOKEN)
			{
				if (emptySpace(1, i))
				{
					board[1][i] = COMPUTER_TOKEN;
					return;
				}
			}
		}

		//If there is no match -> scan and put the first free space
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				if (emptySpace(i, j))
				{
					board[i][j] = COMPUTER_TOKEN;
					return;
				}
			}
		}
	}

	void TicTacToe::game_introduction()
	{
		cout << "\n\tTic-tac-toe (also known as Noughts and crosses or Xs and Os) is a game for two";
		cout << "\n\tplayers, X and O, who take turns marking the spaces in a 3" << char(215) << "3 grid.The player whos";
		cout << "\n\tsucceeds in placing three of their marks in a horizontal, vertical, or diagonal";
		cout << "\n\trow wins the game.";
		cout << "\n\tThis tic-tac-toe program plays against the computer. Human player, X, will always";
		cout << "\n\tfirst. Time will be recorded for the fastest and the slowest game. Average time will";
		cout << "\n\tthen be calculated and displayed.\n";
	}

	void TicTacToe::start_game()
	{
		game_introduction();
		char ans = 'N';

		do
		{
			time_t total_game = time(NULL);
			cout << "\n\tGame begins...";
			drawBoard();

			int resultHumanMoved = -1;

			while (true)
			{
				resultHumanMoved = humanMove();

				if (resultHumanMoved == 0)
				{
					cout << "\n\tYou forfeited the game. Therefore, Dumb AI has won.\n";
					break;
				}

				drawBoard();

				if (humanAlreadyMoved >= 2)
				{

					if (checkWin(HUMAN_TOKEN))
					{
						cout << "\n\n\tHuman has won.\n";
						break;
					}
				}

				computerMove();
				drawBoard();

				if (humanAlreadyMoved >= 2)
				{
					if (checkWin(COMPUTER_TOKEN))
					{
						cout << "\n\n\t\Dumb AI has actually won.\n";
						break;
					}
				}

				if (numberOfTokens() == 0)
				{
					cout << "\n\n\tIt's a draw.\n";
					break;
				}
			}

			total_game = static_cast<long int>(time(NULL) - total_game);
			setRecordTimePlayed({ total_game , humanAlreadyMoved });

			ans = toupper(inputChar("\n\tPlay again? (Y-yes or N-no): ", 'Y', 'N'));
			clearBoard();

		} while (ans == 'Y');

		pair<long int, int> fastest_time_moved = getFastestPlayedTime(), slowest_time_moved = getSlowestPlayedTime();
		double avg_time_played = getAveragePlayedTime();

		cout << "\n\tGame statistics:";
		cout << "\n\n\t" << gamePlayed << " " << (gamePlayed > 1 ? " games" : "game") << " of Tic-Tac-Toe were played.";
		cout << "\n\t\tThe fastest time was " << fastest_time_moved.first << (fastest_time_moved.first > 1 ? " seconds" : " second") << " in " << fastest_time_moved.second << (fastest_time_moved.second > 1 ? " moves" : " move");
		cout << "\n\t\tThe slowest time was " << slowest_time_moved.first << (slowest_time_moved.first > 1 ? " seconds" : " second") << " in " << slowest_time_moved.second << (slowest_time_moved.second > 1 ? " moves" : " move");
		cout << "\n\t\tThe average time was " << avg_time_played << (avg_time_played > 1 ? " seconds" : " second") << "\n\n";
	}

	void TicTacToe::drawBoard()
	{
		cout << "\n\n\t\t Tic-Tac-Toe";
		cout << "\n\t\t" << char(201) << string(3, char(205)) << char(203) << string(3, char(205)) << char(203) << string(3, char(205)) << char(187);
		for (int i = 0; i < 3; i++)
		{
			cout << "\n\t\t" << char(186) << " " << board[i][0] << " " << char(186) << " " << board[i][1] << " " << char(186) << " " << board[i][2] << " " << char(186);
			if (i != 2)
				cout << "\n\t\t" << char(204) << string(3, char(205)) << char(206) << string(3, char(205)) << char(206) << string(3, char(205)) << char(185);
		}
		cout << "\n\t\t" << char(200) << string(3, char(205)) << char(202) << string(3, char(205)) << char(202) << string(3, char(205)) << char(188);
	}

	int TicTacToe::humanMove()
	{
		int row;
		int column;
		bool correctMove = false;
		cout << "\n\n\tHuman moves...\n\n";
		while (!correctMove)
		{
			row = inputInteger("\t\tEnter the board's row # (1..3) or 0 to forfeit: ", 0, 3) - 1;
			if (row == -1)
				return 0;

			column = inputInteger("\t\tEnter the board's column # (1..3) or 0 to forfeit:", 0, 3) - 1;

			if (column == -1)
				return 0;

			if (emptySpace(row, column))
			{
				board[row][column] = HUMAN_TOKEN;
				humanAlreadyMoved++;
				return -1;
			}
			else
				cout << "\n\tERROR: Illegal move. The square has already owned. Please re-specify.\n\n";
		}
	}

	// 'O' or 'X' ( COMPUTER_TOKEN or HUMAN_TOKEN )
	bool TicTacToe::checkWin(char player) 
	{
		for (int i = 0; i < 3; i++)	
		{
			if (player == board[i][0] && player == board[i][1] && player == board[i][2]) // vertical
				return true;
			if (player == board[0][i] && player == board[1][i] && player == board[2][i]) // horizontal
				return true;
		}
		if (player == board[0][0] && player == board[1][1] && player == board[2][2]) // right diagnol
			return true;
		if (player == board[0][2] && player == board[1][1] && player == board[2][0]) // left diagonal
			return true;
		return false;
	}

	void TicTacToe::clearBoard()
	{
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				board[i][j] = BLANK_TOKEN;
			}
		}
		humanAlreadyMoved = 0;
		gamePlayed++;
	}

	int TicTacToe::numberOfTokens()
	{
		int count = 0;
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				if (board[i][j] == BLANK_TOKEN)
					count++;
			}
		}
		return count;
	}

	bool TicTacToe::emptySpace(int row, int column)
	{
		return (board[row][column] == BLANK_TOKEN);
	}

	void TicTacToe::setGameplayed()
	{
		gamePlayed++;
	}

	int TicTacToe::getGameplayed() const
	{
		return gamePlayed;
	}

	void TicTacToe::setRecordTimePlayed(pair<long int,int> pair_value)
	{
		record_playedTime_and_Moved.push_back(pair_value);
	}

	pair<long int,int> TicTacToe::getFastestPlayedTime() const
	{
		if (record_playedTime_and_Moved.size() == 1)
			return record_playedTime_and_Moved.at(0);

		int min_index = 0; long int min_value = record_playedTime_and_Moved.at(0).first;

		for (size_t i = 1; i < record_playedTime_and_Moved.size(); i++)
		{
			if (min_value > record_playedTime_and_Moved.at(i).first)
			{
				min_value = record_playedTime_and_Moved.at(i).first;
				min_index = i;
			}
		}

		return record_playedTime_and_Moved.at(min_index);
	}

	pair<long int, int> TicTacToe::getSlowestPlayedTime() const
	{
		if (record_playedTime_and_Moved.size() == 1)
			return record_playedTime_and_Moved.at(0);

		int max_index = 0; long int max_value = record_playedTime_and_Moved.at(0).first;

		for (size_t i = 1; i < record_playedTime_and_Moved.size(); i++)
		{
			if (max_value < record_playedTime_and_Moved.at(i).first)
			{
				max_value = record_playedTime_and_Moved.at(i).first;
				max_index = i;
			}
		}
		return record_playedTime_and_Moved.at(max_index);
	}

	double TicTacToe::getAveragePlayedTime() const
	{
		if (record_playedTime_and_Moved.size() == 1)
			return record_playedTime_and_Moved.at(0).first;

		int sum = 0;
		
		for (int i = 0; i < record_playedTime_and_Moved.size(); i++)
			sum += record_playedTime_and_Moved.at(i).first;

		return sum / static_cast<double>(record_playedTime_and_Moved.size());
	}

}
