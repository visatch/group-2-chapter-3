//Visa Touch, Thien An Phan

#pragma once
#include <iostream>
#include <vector>
#include <utility>

using namespace std;

namespace tictactoeNamespace
{
	class TicTacToe
	{
	private:
		char board[3][3];
		int humanAlreadyMoved;
		int gamePlayed;
		vector<pair<long int,int>> record_playedTime_and_Moved;
	public:
		//Precondition: None
		//Postcondition: Default Constructor
		TicTacToe();
		//Precondition: None
		//Postcondition: Output the introduction to the game
		void game_introduction();
		//Precondition: None
		//Postcondition: Start the game
		void start_game();
		//Precondition: None
		//Postcondition: Draw the board
		void drawBoard();
		//Precondition: None
		//Postcondition: DumbAI makes the move
		void computerMove();
		//Precondition: None
		//Postcondition: Ask the user to input row and column to make the move
		int humanMove();
		//Precondition: None
		//Postcondition: Reset the game board
		void clearBoard();
		//Precondition: char player
		//Postcondition: Return true if the input player is a winner, otherwise return false
		bool checkWin(char player);
		//Precondition: int row, int column
		//Postcondition: Return true if the input row and column is empty; otherwise return false 
		bool emptySpace(int row, int column);
		//Precondition: None
		//Postcondition: Return the number of available space on the game board
		int numberOfTokens();
		//Precondition: None
		//Postcondition: Increase the gamePlayed by one (everytime the function is called)
		void setGameplayed();
		//Precondition: None
		//Postcondition: Return the number of gameplayed
		int getGameplayed() const;
		//Precondition: pair<long int, int> pair_value;
		//Postcondition: Set the duration of the user is played and the number of human makes a move
		void setRecordTimePlayed(pair<long int, int> pair_value);
		//Precondition: None
		//Postcondition: Return a pair value of fastest game play, first -> duration, second -> number of moves
		pair<long int, int> getFastestPlayedTime() const;
		//Precondition: None
		//Postcondition: Return a pair value of slowest game play, first -> duration, second -> number of moves
		pair<long int, int> getSlowestPlayedTime() const;
		//Precondition: None
		//Postcondition: Return the average duration game play
		double getAveragePlayedTime() const;
	};
}
