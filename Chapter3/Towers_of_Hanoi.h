//Geovanny Casian, Angie Campos

#pragma once
#include "Hanoi_tower.h"

namespace hanoiNamespace
{
	class Towers_of_Hanoi
	{
	public:
		// precondition: none
		// postcondition: initialize game; initialize towers with user input
		Towers_of_Hanoi();

		// precondition: none
		// postcondition: bool to check if move is legal
		// sets gameTime equal to time taken to play game and isWin to true if a game was won
		bool move_disk(time_t& gameTime, bool& isWin);

		// precondition: none
		// postcondition: print out towers with disks
		void print_tower();

		// precondition: none
		// postcondition: check if player wants to play again
		bool play_again();
	private:
		Hanoi_Tower One;
		Hanoi_Tower Two;
		Hanoi_Tower Three;
		int disk_this_game;
	};
}

