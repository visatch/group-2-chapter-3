#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

namespace inputNamespace
{
	//return an input char
	char inputChar(string prompt, string listChars);
	//return an input char of y or n
	char inputChar(string prompt, char yes, char no);
	//return an input char
	char inputChar(string prompt, bool alphaOrDigit);
	//return an input char
	char inputChar(string prompt);
	//return an input interger
	int inputInteger(string prompt);
	//return an integer where posNeg is positive (true) or negative (false)
	int inputInteger(string prompt, bool posNeg);
	//return an input integer within range ,start and end
	int inputInteger(string prompt, int startRange, int endRange);
	//return an input double
	double inputDouble(string prompt);
	//return a double where posNeg is positive (true) or negative (false)
	double inputDouble(string prompt, bool posNeg);
	//return an input double within range ,start and end
	double inputDouble(string prompt, double startRange, double endRange);
	bool getBool(string prompt);
	string getLine(string prompt);
	string inputString(string prompt, bool getLine);
}

