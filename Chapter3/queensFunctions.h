//Manh Khang Nguyen, Uriel Renteria

#pragma once
#include <iostream>
#include "nQueens.h"
#include <ctime>
#include "input.h"

namespace queensNamespace
{
	// Precondition(s): None
	// Postcondition(s): Runs nQueen game
	void playGame(nQueens& currentGame, time_t& gameTime, bool& isWin);

	// Precondition(s): None
	// Postcondition(s): A menu to ask user to proceed to game steps
	char gameMenuOption(void);

	// Precondition(s): None
	// Postcondition(s): Asks user to place a queen
	void place_a_queen(nQueens& object);

	// Precondition(s): None
	// Postcondition(s): Asks user to remove a queen
	void remove_a_queen(nQueens& object);
}