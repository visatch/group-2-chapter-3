#include "Hanoi_tower.h"
#include <iostream>
#include <string>
using namespace std;

namespace hanoiNamespace
{
	Hanoi_Tower::Hanoi_Tower()
	{

	}

	Hanoi_Tower::Hanoi_Tower(int disk_amount)
	{
		for (int itr = disk_amount; itr > 0; --itr)
		{
			disks.push_back(itr);

		}
		amount_of_disks = disk_amount;
	}

	Hanoi_Tower::~Hanoi_Tower()
	{

	}

	int Hanoi_Tower::top_disk_value(void)const
	{
		return disks.back();
	}

	void Hanoi_Tower::remove_top_disk(void)
	{
		disks.pop_back();
		--amount_of_disks;

	}

	bool Hanoi_Tower::is_tower_empty(void)
	{
		return (disks.empty());
	}

	void Hanoi_Tower::add_disk_to_top(int new_disk)
	{
		disks.push_back(new_disk);
		++amount_of_disks;
	}

	bool Hanoi_Tower::can_place_disk(int new_disk)
	{

		if (disks.empty())
		{
			return true;

		}
		else
		{
			if (new_disk < disks.back())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	bool Hanoi_Tower::win_condition(int disks_in_game) const
	{
		return (disks_in_game == disks.size());
	}

	int Hanoi_Tower::disk_at_location(int location)const
	{
		return (disks[location]);

	}

	int Hanoi_Tower::disk_on_tower()const
	{
		return disks.size();
	}

	void Hanoi_Tower::empty_tower()
	{
		disks.clear();
		amount_of_disks = 0;
	}
}