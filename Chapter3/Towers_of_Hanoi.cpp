#include "Towers_of_Hanoi.h"
#include <string>
#include <iostream>
#include <ctime>
#include "Tools.h"

namespace hanoiNamespace
{
    Towers_of_Hanoi::Towers_of_Hanoi()
    {
        cout << "\n\tThe Tower of Hanoi also called the Tower of Brahma or Lucas' Tower is a mathematical game." << endl;
        cout << "\tIt consists of three pegs and a number of rings of different sizes, which can slide onto" << endl;
        cout << "\tany peg.The game starts with the rings in a neat stack in ascending order of size on one" << endl;
        cout << "\tpeg, the smallest at the top, thus making a conical shape." << endl;

        cout << "\n\tThe objective of the game is to move the entire stack from the starting peg-A to ending peg-C," << endl;
        cout << "\tobeying the following simple rules : " << endl;

        cout << "\n\t\t1. Only One Disk Can Be Moved At A Time." << endl;
        cout << "\t\t2. Each move consists of taking the upper disk from one of the stacks and" << endl;
        cout << "\t\tplacing it on top of another stack or on an empty peg." << endl;
        cout << "\t\t3. No larger disk may be placed on top of a smaller disk.\n" << endl;

        disk_this_game = getBoundedInt("\n\tEnter the number of rings (1..64) to begin: ", 1, 64);

        for (int itr = disk_this_game; itr > 0; --itr)
        {
            One.add_disk_to_top(itr);
        }
    }

    bool Towers_of_Hanoi::move_disk(time_t& gameTime, bool& isWin)
    {
        time_t endTime, startTime = time(0);
        isWin = false;

        int starting_peg = getBoundedInt("\tSelect the top disk from the start peg (1,2,3, or 4 to Quit) ", 1, 4);
        int ending_peg = getBoundedInt("\tSelect the end peg (1,2,3, or 4 to Quit) ", 1, 4);
        Hanoi_Tower* tower_ptr_start = nullptr;
        Hanoi_Tower* tower_ptr_end = nullptr;

        if (starting_peg == 4 || ending_peg == 4)
        {
            return false;
        }

        if (starting_peg != ending_peg)
        {
            switch (starting_peg)
            {
            case 1:tower_ptr_start = &One; break;
            case 2:tower_ptr_start = &Two; break;
            case 3:tower_ptr_start = &Three; break;
            default: break;
            }
            switch (ending_peg)
            {
            case 1:tower_ptr_end = &One; break;
            case 2:tower_ptr_end = &Two;  break;
            case 3:tower_ptr_end = &Three;  break;
            default: break;
            }

            if (tower_ptr_start->is_tower_empty())
            {
                cout << "\ntERROR: Cannot make the move. There is no disk in the selected peg- " << starting_peg << "." << endl;
                cout << "\tPlease choose again." << endl;
            }
            else
            {
                if (tower_ptr_end->can_place_disk(tower_ptr_start->top_disk_value()))
                {
                    int temp_disk_value = tower_ptr_start->top_disk_value();
                    tower_ptr_end->add_disk_to_top(temp_disk_value);
                    tower_ptr_start->remove_top_disk();
                }
                else
                {
                    cout << "\ntERROR: Cannot make the move. Top disk (" << tower_ptr_start->top_disk_value() << ") of peg-" << starting_peg << ", is larger than top disk(" << tower_ptr_end->top_disk_value() << ") of peg-" << ending_peg << '.' << endl;
                    cout << "\tPlease choose again." << endl;
                }
            }
        }

        else
        {
            cout << "\ntERROR: Cannot make the move. The selected end peg cannot be the same as the selected start peg." << endl;
            cout << "\tPlease choose again." << endl;
        }

        //WIN CONDITION CHECK
        if (Three.win_condition(disk_this_game))
        {
            this->print_tower();
            cout << "\n\tGAME OVER, YOU WIN!!!" << endl;
            endTime = time(0);
            gameTime = (endTime - startTime);
            isWin = true;
            return false;
        }
        return true;
    }

    void Towers_of_Hanoi::print_tower()
    {
        cout << endl;
        for (int itr = disk_this_game; itr >= 0; --itr)
        {
            if (itr == 0)
            {
                cout << "\t" << static_cast<char>(205);
                cout << static_cast<char>(205);
                cout << static_cast<char>(205);
                cout << static_cast<char>(202);
                cout << static_cast<char>(205);
                cout << static_cast<char>(205);
                cout << static_cast<char>(205);
            }
            else if (One.disk_on_tower() >= itr && One.disk_on_tower() != 0)
            {
                cout << "\t   " << One.disk_at_location(itr - 1);
            }
            else
            {
                cout << "\t   " << static_cast<char>(186);
            }

            if (itr == 0)
            {
                cout << "\t" << static_cast<char>(205);
                cout << static_cast<char>(205);
                cout << static_cast<char>(205);
                cout << static_cast<char>(202);
                cout << static_cast<char>(205);
                cout << static_cast<char>(205);
                cout << static_cast<char>(205);
            }
            else if (Two.disk_on_tower() >= itr && Two.disk_on_tower() != 0)
            {
                cout << "\t   " << Two.disk_at_location(itr - 1);
            }
            else
            {
                cout << "\t   " << static_cast<char>(186);
            }

            if (itr == 0)
            {
                cout << "\t" << static_cast<char>(205);
                cout << static_cast<char>(205);
                cout << static_cast<char>(205);
                cout << static_cast<char>(202);
                cout << static_cast<char>(205);
                cout << static_cast<char>(205);
                cout << static_cast<char>(205) << endl;
            }
            else if (Three.disk_on_tower() >= itr && Three.disk_on_tower() != 0)
            {
                cout << "\t   " << Three.disk_at_location(itr - 1) << endl;
            }
            else
            {
                cout << "\t   " << static_cast<char>(186) << endl;
            }
        }
    }

    bool Towers_of_Hanoi::play_again()
    {
        cout << "\n\tDo You Wish To Play Again?" << endl;
        char game_continue = getYesNo("\tEnter Y or N: ");

        if (game_continue == 'y' || game_continue == 'Y')
        {
            return true;
        }

        else if (game_continue == 'n' || game_continue == 'N')
        {

            return false;
        }

    }
}

